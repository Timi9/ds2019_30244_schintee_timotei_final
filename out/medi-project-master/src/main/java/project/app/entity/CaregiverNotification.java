package project.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CaregiverNotification {
    @Id
    @GeneratedValue
    private int id;

    private String notification;
    private int patientId;

    public CaregiverNotification(String notification, int patientId) {
        this.notification = notification;
        this.patientId = patientId;
    }

    public CaregiverNotification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }
}
