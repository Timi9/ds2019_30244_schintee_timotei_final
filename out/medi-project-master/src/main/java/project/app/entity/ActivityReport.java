package project.app.entity;

public class ActivityReport {
    private String activity;
    private int nr;

    public ActivityReport(String activity, int nr) {
        this.activity = activity;
        this.nr = nr;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }
}
