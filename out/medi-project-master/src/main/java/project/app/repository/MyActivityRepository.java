package project.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.app.entity.MyActivity;

public interface MyActivityRepository extends JpaRepository<MyActivity, Integer> {

}
