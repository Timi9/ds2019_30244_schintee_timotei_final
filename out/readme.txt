===> SETUP
 - MySQL Database (v. 5.7) ->  create database and put database name in in src/main/resources/application.yml at url: jdbc:mysql://localhost:3306/DB_NAME
 - Java 8
 - Intellij
 - Lombok -> plugin de Intellij: Settings -> Plugins -> search lombok
 - Nodejs (dupa install, "npm install -g @angular/cli")


===> STARTING
 - Server -> from Intellij
 - Client -> from terminal, go to client directory and "npm start"
 
===> DEPLOY
MAVEN: click on Package in Maven menu to create .jar file
GRADLE: click on Build and buildJar in Gradle menu to create .jar file

docker build -t app-image.
docker build -t med-image.

docker run 
	--name mysql-standalone 
	-e MYSQL_ROOT_PASSWORD=admin 
	-e MYSQL_DATABASE=hospital 
	-e MYSQL_USER=user 
	-e MYSQL_PASSWORD=admin 
	-d mysql
	
OR 

docker-compose up --build


