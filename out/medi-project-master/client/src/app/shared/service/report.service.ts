import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {StoreService} from "./store.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {ActivityReport} from "../model/activity.report";
import {CaregiverNotification} from "../model/caregiver.notification";
import {MedicationTaken} from "../model/medication.taken";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private http: HttpClient, private auth: AuthService, private store: StoreService, private router: Router) {
  }

  fetchActivityReport(): Observable<ActivityReport[]> {
    return this.http.get<ActivityReport[]>('api/activity/report', {headers: {Authorization: this.auth.getToken()}});
  }

  fetchNotifications(): Observable<CaregiverNotification[]> {
    return this.http.get<CaregiverNotification[]>('api/activity/notifications', {headers: {Authorization: this.auth.getToken()}});
  }
  fetchMedTaken(): Observable<MedicationTaken[]> {
    return this.http.get<MedicationTaken[]>('api/activity/medications', {headers: {Authorization: this.auth.getToken()}});
  }
}
