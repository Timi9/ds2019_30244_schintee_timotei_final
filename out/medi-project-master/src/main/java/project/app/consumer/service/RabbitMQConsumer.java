package project.app.consumer.service;


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import project.app.consumer.model.Activity;
import project.app.entity.CaregiverNotification;
import project.app.entity.MyActivity;
import project.app.repository.CaregiverNotificationRepository;
import project.app.repository.MyActivityRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


@Component
public class RabbitMQConsumer {
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private MyActivityRepository myActivityRepository;
    @Autowired
    private CaregiverNotificationRepository caregiverNotificationRepository;

    @Bean
    public Jackson2JsonMessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @RabbitListener(queues = "${javainuse.rabbitmq.queue}")
    public void recievedMessage(Activity activity) {
        System.out.println("[Received Message From RabbitMQ] : " + activity.getActivityName() + " " + activity.getStartTime() + " " + activity.getEndTime());

        String name = activity.getActivityName();
        LocalDateTime startTime = activity.getStartTime();
        LocalDateTime endTime = activity.getEndTime();

        MyActivity myActivity = new MyActivity(startTime, endTime, name, 3);
        myActivityRepository.save(myActivity);


        /**
         *  R1
         */
        if (name.replaceAll("\\s+", "").equalsIgnoreCase("Leaving")) {
            long hours = ChronoUnit.HOURS.between(startTime, endTime);
            if (hours > 5) {
                System.out.println(" The leaving activity is longer than 12 hours");
                template.convertAndSend("/chat", "The leaving activity is longer than 12 hours");
                CaregiverNotification caregiverNotification = new CaregiverNotification("The leaving activity is longer than 12 hours",3);
                caregiverNotificationRepository.save(caregiverNotification);
            }
        }

        /**
         *   R2
         */
        if (name.replaceAll("\\s+", "").equalsIgnoreCase("Sleeping")) {
            long hours = ChronoUnit.HOURS.between(startTime, endTime);
            if (hours > 5) {
                System.out.println(" The sleep period longer than 12 hours");
                template.convertAndSend("/chat", "The sleep period longer than 12 hours");
                CaregiverNotification caregiverNotification = new CaregiverNotification("The sleep period longer than 12 hours",3);
                caregiverNotificationRepository.save(caregiverNotification);
            }

        }

        /**
         *   R3
         */
        if (name.replaceAll("\\s+", "").equalsIgnoreCase("Toileting")) {
            long seconds = ChronoUnit.SECONDS.between(startTime, endTime);
            if (seconds > 30) {
                System.out.println(" The period spent in bathroom is longer than 1 hour");
                template.convertAndSend("/chat", "The period spent in bathroom is longer than 1 hour");
                CaregiverNotification caregiverNotification = new CaregiverNotification("The period spent in bathroom is longer than 1 hour",3);
                caregiverNotificationRepository.save(caregiverNotification);
            }
        }

        if (name.replaceAll("\\s+", "").equalsIgnoreCase("Showering")) {
            long seconds = ChronoUnit.SECONDS.between(startTime, endTime);
            if (seconds > 30) {
                System.out.println(" The period spent in bathroom is longer than 1 hour");
                template.convertAndSend("/chat", "The period spent in bathroom is longer than 1 hour");
                CaregiverNotification caregiverNotification = new CaregiverNotification("The period spent in bathroom is longer than 1 hour",3);
                caregiverNotificationRepository.save(caregiverNotification);
            }
        }


    }
}