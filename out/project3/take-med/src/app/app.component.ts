import {Component, OnInit, OnDestroy} from '@angular/core';
import { timer, Subscription } from 'rxjs';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  info:String;
  clock:Date ;
  year;
  month;
  day;
  hour;
  second;
  minute;

  constructor() {
    this.clock = new Date(2019,11,10,0,0,0);
    // Runs the enclosed function on a set interval.
    setInterval(() => {         //replaced function() by ()=>
      this.increaseTime();
      //console.log(this.clock); // just testing if it is working
    }, 0.1);
  }

  increaseTime(){
    this.clock.setSeconds(this.clock.getSeconds()+1);

    this.year=this.clock.getFullYear();
    this.month=this.clock.getMonth();
    this.day=this.clock.getDate();
    this.hour = this.clock.getHours();
    this.second=this.clock.getSeconds();
    this.minute=this.clock.getMinutes();
  }


}

