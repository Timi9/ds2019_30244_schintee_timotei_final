package producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import producer.model.Activity;
import producer.service.RabbitMQSender;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Component
public class Producer implements ApplicationRunner {
    @Autowired
    RabbitMQSender rabbitMQSender;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        File file;

        if (Files.exists(Paths.get("./activities.txt"))) {
            file = new File("./activities.txt");
        } else {
            file = new File(this.getClass().getClassLoader().getResource("activities.txt").getFile());
        }

        System.out.println("da");
        Scanner sc = null;
        Scanner s = null;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        while (sc.hasNext()) {
            String line = sc.nextLine();
            if (line == null)
                break;

            s = new Scanner(line);
            s.useDelimiter("\\t\\t");

            LocalDateTime startTime = LocalDateTime.parse(s.next(), formatter);
            LocalDateTime endTime = LocalDateTime.parse(s.next(), formatter);
            String activity_name = s.next();

            Activity activity = new Activity();

            activity.setStartTime(startTime);
            activity.setEndTime(endTime);
            activity.setActivityName(activity_name);

            rabbitMQSender.send(activity);
            TimeUnit.SECONDS.sleep(1);

        }

    }
}
