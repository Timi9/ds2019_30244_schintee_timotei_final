package project.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.app.entity.ActivityReport;
import project.app.entity.CaregiverNotification;
import project.app.entity.PatientMedicationTaken;
import project.app.service.ActivityService;

import java.util.List;

@RestController
@RequestMapping("/api/activity")
@RequiredArgsConstructor
public class ActivityController {
    @Autowired
    private ActivityService activityService;

    @GetMapping("/report")
    public List<ActivityReport> getReport() {
        return activityService.getRaport();
    }

    @GetMapping("notifications")
    public List<CaregiverNotification> getNotif() {
        return activityService.getNotif();
    }

    @GetMapping("medications")
    public List<PatientMedicationTaken> getMedicaions() {
        return activityService.getMedTaken();
    }
}
