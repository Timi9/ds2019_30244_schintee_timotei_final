import {Resolve} from "@angular/router";
import {Injectable} from "@angular/core";
import {ReportService} from "./report.service";

@Injectable()
export class RaportResolver implements Resolve<any> {
  constructor(private reportService: ReportService) {

  }

  resolve() {
    return this.reportService.fetchActivityReport()
  }
}
