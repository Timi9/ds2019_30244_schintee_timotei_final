package project.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.app.entity.CaregiverNotification;

public interface CaregiverNotificationRepository extends JpaRepository<CaregiverNotification, Integer> {

}
