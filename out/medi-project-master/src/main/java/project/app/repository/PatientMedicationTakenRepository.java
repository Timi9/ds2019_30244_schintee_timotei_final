package project.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.app.entity.PatientMedicationTaken;

public interface PatientMedicationTakenRepository  extends JpaRepository<PatientMedicationTaken, Integer> {
}
