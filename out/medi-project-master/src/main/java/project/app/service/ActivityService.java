package project.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.app.entity.ActivityReport;
import project.app.entity.CaregiverNotification;
import project.app.entity.MyActivity;
import project.app.entity.PatientMedicationTaken;
import project.app.repository.CaregiverNotificationRepository;
import project.app.repository.MyActivityRepository;
import project.app.repository.PatientMedicationTakenRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityService{
    @Autowired
    private MyActivityRepository myActivityRepository;
    @Autowired
    private CaregiverNotificationRepository caregiverNotificationRepository;
    @Autowired
    private PatientMedicationTakenRepository patientMedicationTakenRepository;

    private static int k=0;

    public List<ActivityReport> getRaport() {
        List<ActivityReport> reports = new ArrayList<>();

        Iterable<MyActivity> activities = myActivityRepository.findAll();
        List<MyActivity> myActivities = new ArrayList<>();
        activities.forEach(myActivities::add);
        System.out.println("nr of activities "+myActivities.size());
        List<String> names = new ArrayList<>();

        for (MyActivity a : myActivities) {
            if (!names.contains(a.getName()))
                names.add(a.getName());
        }


        for (String s : names) {
            int k=0;
            for (MyActivity activity: myActivities)
                if (containsIgnoreCase(s,activity.getName()))
                    k++;

            reports.add(new ActivityReport(s,k));
        }

        return reports;
    }
    public static boolean containsIgnoreCase(String str, String subString) {
        return str.toLowerCase().contains(subString.toLowerCase());
    }

    public List<CaregiverNotification> getNotif(){
        return caregiverNotificationRepository.findAll();
    }

    public List<PatientMedicationTaken> getMedTaken(){
       if (k==0) dbadd();
       k++;
       return patientMedicationTakenRepository.findAll();
    }

    private void dbadd(){

        PatientMedicationTaken p1 = new PatientMedicationTaken("Patient did not take Paracetamol");
        PatientMedicationTaken p2= new PatientMedicationTaken("Patient take Visine");
        PatientMedicationTaken p3 = new PatientMedicationTaken("Patient take Ibuprofen");
        PatientMedicationTaken p4 = new PatientMedicationTaken("Patient did not take NoSPA");
        PatientMedicationTaken p5 = new PatientMedicationTaken("Patient did not take Sinupret");

        patientMedicationTakenRepository.save(p1);
        patientMedicationTakenRepository.save(p2);
        patientMedicationTakenRepository.save(p3);
        patientMedicationTakenRepository.save(p4);
        patientMedicationTakenRepository.save(p5);

    }
}
