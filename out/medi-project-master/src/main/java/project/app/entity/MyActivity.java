package project.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class MyActivity {
    @Id
    @GeneratedValue
    private int id;

    private LocalDateTime start;
    private LocalDateTime end;
    private String name;
    private long patient_id;

    public MyActivity( LocalDateTime start, LocalDateTime end, String name, long patient_id) {
        this.start = start;
        this.end = end;
        this.name = name;
        this.patient_id = patient_id;
    }

    public MyActivity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(long patient_id) {
        this.patient_id = patient_id;
    }
}
